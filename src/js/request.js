import axios from 'axios';
import { ElMessage } from 'element-plus';
import { Session } from './storage';

const service = axios.create({// 配置新建一个 axios 实例
	baseURL: (window).g.ApiUrl,
	timeout: 50000,
	withCredentials: true,
	headers: { 'Content-Type': 'application/json' },
});

service.interceptors.request.use(//添加请求拦截器
	(config) => {
		if (Session.get('token')) {//携带token请求头
      // config.headers.common['Authorization'] = `${Session.get('token')}`;
      // config.headers.token = Session.get('token');
		}
    // let userInfo = Session.get('userInfo');
    // if(userInfo!=null){
    //   config.headers.customerId = 
    // }
		return config;
	},
	(error) => {
		return Promise.reject(error);// 对请求错误做些什么
	}
);

service.interceptors.response.use(// 添加响应拦截器
	(response) => {
		const res = response.data;		
		if (res == 401) {//登录过期
			if (Session.get('token')) {
				ElMessage({type: 'error', message: '登录过期，请重新登录', duration: 6 * 1000})
			}
			Session.clear(); // 清除浏览器全部临时缓存
			window.location.href = '/'; // 去登录页
			return Promise.reject(service.interceptors.response);
		}else if (res == 402) {//账户同时登录时
			if (Session.get('token')) {
				ElMessage({type: 'error', message: '您的账户已在其他地方登录，请重新登录',duration: 6 * 1000})
			}
			Session.clear(); // 清除浏览器全部临时缓存
			window.location.href = '/';
			return Promise.reject(service.interceptors.response);
		}else if (res.code && res.code !== 200) {// `token` 过期或者账号已在别处登录
			if (res.code === 500) {
				ElMessage({type: 'error',message: res.msg,});
			}
			return Promise.reject(service.interceptors.response);
		} else {
			return response.data;
		}
	},
	(error) => {// 对响应错误做点什么
		if (error.message.indexOf('timeout') != -1) {
			ElMessage.error('网络超时');
		} else if (error.message == 'Network Error') {
			ElMessage.error('网络连接错误');
		} else {
      console.log('error=',error);
			if (error.response.data) ElMessage.error(error.response.statusText);
			else ElMessage.error('接口路径找不到');
		}
		return Promise.reject(error);
	}
);

export default service;
