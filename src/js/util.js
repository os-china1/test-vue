//工具集
export function strToArr(str){//字符串转为list 示例：0-图片验证,1-短信验证,2-无验证
  let strs = str.split(',');
  let items= [];
  strs.forEach(element => {
    let splits= element.split('-');
    let item = {
      name: splits[1],
      value: splits[0]
    }
    items.push(item);
  });
  return items;
}

export function exportExcel(res,fileName){//导出文件
  const link = document.createElement("a");
  const blob = new Blob([res], {type: "application/vnd.ms-excel;charset=utf-8"});
  link.style.display = "none";
  link.href = URL.createObjectURL(blob);
  link.setAttribute("download", fileName);
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}

export function downloadFile(row){//导出文件
  const link = document.createElement("a");
  link.style.display = "none";
  link.href = window.g.ApiUrl+row.downloadPath;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}