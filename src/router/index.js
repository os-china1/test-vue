import { createRouter, createWebHashHistory} from 'vue-router'
import {getUserMenu} from '../views/sys/menu'
import {myPinia} from '@/stores';

const routes = [
  {
    name: 'login',
    path: '/',
    meta: { title: '登录', isKeepAlive: true},
    component: () => import('@/views/sys/login/index.vue'),
  },{
    name: 'home',
    path: '/home',
    meta: { title: '主页' },
    component: () => import('@/views/sys/home/index.vue'),
    children:[]
  },
]

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: routes
})

//路由前置守卫处理
router.beforeEach(async(to,from,next)=>{
  if(to.path=='/'){//登录
    console.log("是登录")
    next();
  }else{
    let pinia = myPinia();//.setRoutes()
    if(pinia.routes==undefined || pinia.routes.length==0){//没有缓存路由
      let res = await getUserMenu(pinia.userInfos);
      console.log("res.data")
      console.log(res.data)
      pinia.setRoutes(res.data);//缓存请求回来的路由数据
      let homeRoute = routes.filter(route=>route.path=='/home')[0];//filter()方法返回一个数组。
      let breadcrumbs = [];//存储所有面包屑key-value
      breadcrumbs.push(
        { 
          path:'/home', 
          titles:[{path:'/home',title:'主页'}]
        }
      );
      let modules = import.meta.glob('../views/**/*.vue')
      res.data.forEach(sub=>{//第一层循环-子菜单
        sub.children.forEach(item=>{//第二层循环-菜单条目,这样将菜单扁平化处理
          homeRoute.children.push({
            name: item.name,
            path: item.path,
            meta: { title: item.title},
            component: modules[`../views/${item.component}.vue`],
          })
          breadcrumbs.push(//记录所有面包菜单条目的面包屑
            { 
              path: item.path, 
              titles:[
                {path:sub.path,title:sub.title},
                {path:item.path,title:item.title}
              ]
            }
          );
        })
      })
      pinia.setBreadcrumbs(breadcrumbs);
      router.addRoute(homeRoute);
      next({path:to.path});
    }else{//已经缓存
      next();
    }
    let breadcrumb = pinia.breadcrumbs.find(i=>i.path==to.path);
    pinia.setTitles(breadcrumb.titles);
  }
})

export default router