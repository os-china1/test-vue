import request from '@/js/request';
//查询菜单
export function pageMenu(params) {
	return request({url: '/api/sys/menu/page', method: 'post', data: params });
}

//保存菜单
export function saveMenu(params) {
	return request({url: '/api/sys/menu/save',method: 'post',data: params});
}

//删除菜单
export function delMenu(params) {
	return request({url: '/api/sys/menu/del',method: 'post',data: params});
}

//查询用户菜单
export function getUserMenu(params) {
	return request({url:'/api/sys/menu/listMenusByUser',method: 'post',data: params,});
}