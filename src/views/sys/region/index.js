import request from '@/js/request';

//加载省市区
export function listRegion(params) {
	return request({url: '/api/sys/region/list',method: 'post',data: params});
}