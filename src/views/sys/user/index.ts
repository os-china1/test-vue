import request from '../../../js/request';//params?参数拼接在url  params: + data: params,参数不会拼接在url（根据情况选择）
//查询用户
export function pageUser(params) {
  return request({url:'/api/sys/user/page',method:'post', data:params,});
}

//删除用户
export function delUser(params) {
	return request({url: '/api/sys/user/del',method: 'post',data: params});
}

//保存用户
export function saveUser(params) {
  return request({url: '/api/sys/user/save',method: 'post',data: params})
}

//重置密码
export function resetPassword(params) {
	return request({url: '/api/sys/user/resetPassword',method: 'post',data: params,});
}

//修改密码
export function updatePassword(params) {
  return request({url: '/api/sys/user/updatePassword',method: 'post',data: params,});
}