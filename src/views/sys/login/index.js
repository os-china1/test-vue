import request from '@/js/request';

// 用户登录
export function signIn(params) {
  // let custId = params.custId;
  // let username = custId+params.username;//验证时使用custId+username发往后端
  // params.username = username;
  return request({
    url: '/authentication/form',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
    transformRequest: [
      function (data) {
        let url = '';
        for (let k in data) {
          let value = data[k] != undefined ? data[k] : '';
          url += `&${k}=${encodeURIComponent(value)}`;
        }
        return url ? url.substring(1) : '';
      }
    ]
  });
}