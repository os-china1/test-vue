import request from '../../../js/request';

//查询角色
export function pageRole(params) {
	return request({url: '/api/sys/role/page', method: 'post', data: params });
}

//保存角色
export function saveRole(params) {
	return request({url: '/api/sys/role/save',method: 'post',data: params});
}

//删除角色
export function delRole(params) {
	return request({url: '/api/sys/role/del',method: 'post',data: params});
}

//角色菜单列表
export function listRoleMenu(params) {
	return request({url: '/api/sys/role/menuListByRoleId',method: 'post',data: params});
}

//保存角色菜单
export function saveRoleMenu(params) {
	return request({url: '/api/sys/role/saveRoleMenu',method: 'post',data: params});
}

//修改角色菜单
export function updateRoleMenu(params) {
	return request({url: '/api/sys/role/saveRoleMenus',method: 'post',data: params});
}