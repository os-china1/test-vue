import request from '@/js/request';

//查询参数
export function pageDict(params) {
	return request({url: '/api/sys/dict/page', method: 'post', data: params });
}

//保存参数
export function saveDict(params) {
	return request({url: '/api/sys/dict/save',method: 'post',data: params});
}

//删除参数
export function delDict(params) {
	return request({url: '/api/sys/dict/del',method: 'post',data: params});
}