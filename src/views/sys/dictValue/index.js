import request from '@/js/request';

//查询参数
export function pageDictValue(params) {
	return request({url: '/api/sys/dict-value/page', method: 'post', data: params });
}

//保存参数
export function saveDictValue(params) {
	return request({url: '/api/sys/dict-value/save',method: 'post',data: params});
}

//删除参数
export function delDictValue(params) {
	return request({url: '/api/sys/dict-value/del',method: 'post',data: params});
}

//查询字典值列表
export function listByName(params) {
	return request({url: '/api/sys/dict-value/listByName',method: 'post',data: params});
}