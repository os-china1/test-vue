import request from '@/js/request';

//查询单身者
export function pageSingle(params) {
	return request({url: '/api/mytry/single/page', method: 'post', data: params });
}

//保存单身者
export function saveOrUpdateSingle(params) {
	return request({url: '/api/mytry/single/saveOrUpdate',method: 'post',data: params});
}

//删除单身者
export function delSingle(params) {
	return request({url: '/api/mytry/single/del',method: 'post',data: params});
}