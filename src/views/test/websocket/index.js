import request from '@/js/request';

//查询参数
export function pageParam(params) {
	return request({url: '/api/sys/param/page', method: 'post', data: params });
}

//保存参数
export function saveParam(params) {
	return request({url: '/api/sys/param/save',method: 'post',data: params});
}

//删除参数
export function delParam(params) {
	return request({url: '/api/sys/param/del',method: 'post',data: params});
}