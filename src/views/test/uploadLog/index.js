import request from '@/js/request';

//查询UploadLog
export function pageUploadLog(params) {
	return request({url: '/api/test/uploadLog/page', method: 'post', data: params });
}

//保存UploadLog，上传文件接口，
export function saveUploadLog() {
	return '/api/test/uploadLog/upload';
}

//删除UploadLog
export function delUploadLog(params) {
	return request({url: '/api/test/uploadLog/del',method: 'post',data: params});
}

//导出UploadLog
export function exportUploadLog(params) {
	return request({url: '/api/test/uploadLog/export',method: 'post',data: params, responseType: 'blob'});
}