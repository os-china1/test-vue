import request from '@/js/request';

//查询项目联系人
export function pageProjectProduct(params) {
	return request({url: '/api/sal/project-product/page', method: 'post', data: params });
}

//保存项目联系人
export function saveProjectProduct(params) {
	return request({url: '/api/sal/project-product/save',method: 'post',data: params});
}

//删除项目联系人
export function delProjectProduct(params) {
	return request({url: '/api/sal/project-product/del',method: 'post',data: params});
}