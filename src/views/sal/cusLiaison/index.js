import request from '@/js/request';

//查询销售客户联系人
export function pageCusLiaison(params) {
	return request({url: '/api/sal/cus-liaison/page', method: 'post', data: params });
}

//保存销售客户联系人
export function saveCusLiaison(params) {
	return request({url: '/api/sal/cus-liaison/save',method: 'post',data: params});
}

//删除销售客户联系人
export function delCusLiaison(params) {
	return request({url: '/api/sal/cus-liaison/del',method: 'post',data: params});
}