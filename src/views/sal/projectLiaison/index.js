import request from '@/js/request';

//查询项目联系人
export function pageProjectLiaison(params) {
	return request({url: '/api/sal/project-liaison/page', method: 'post', data: params });
}

//保存项目联系人
export function saveProjectLiaison(params) {
	return request({url: '/api/sal/project-liaison/save',method: 'post',data: params});
}

//删除项目联系人
export function delProjectLiaison(params) {
	return request({url: '/api/sal/project-liaison/del',method: 'post',data: params});
}