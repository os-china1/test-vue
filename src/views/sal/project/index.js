import request from '@/js/request';

//查询项目
export function pageProject(params) {
	return request({url: '/api/sal/project/page', method: 'post', data: params });
}

//保存项目
export function saveProject(params) {
	return request({url: '/api/sal/project/save',method: 'post',data: params});
}

//删除项目
export function delProject(params) {
	return request({url: '/api/sal/project/del',method: 'post',data: params});
}