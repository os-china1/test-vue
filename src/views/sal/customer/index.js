import request from '@/js/request';

//查询客户
export function pageCustomer(params) {
	return request({url: '/api/sal/customer/page', method: 'post', data: params });
}

//保存客户
export function saveCustomer(params) {
	return request({url: '/api/sal/customer/save',method: 'post',data: params});
}

//删除客户
export function delCustomer(params) {
	return request({url: '/api/sal/customer/del',method: 'post',data: params});
}