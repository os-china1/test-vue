import request from '@/js/request';

//查询项目
export function pageProduct(params) {
	return request({url: '/api/sal/product/page', method: 'post', data: params });
}

//保存项目
export function saveProduct(params) {
	return request({url: '/api/sal/product/save',method: 'post',data: params});
}

//删除项目
export function delProduct(params) {
	return request({url: '/api/sal/product/del',method: 'post',data: params});
}