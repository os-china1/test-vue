import { createApp } from 'vue'
import { createPinia } from 'pinia' //按需引入

import router from './router'  //从./router目录下引入export的router
import ElementPlus from 'element-plus' //全局引入element-plus
import 'element-plus/dist/index.css' //引入element-plus样式
import axios from 'axios'

import './assets/main.css'
import zhCn from 'element-plus/lib/locale/lang/zh-cn'//element-plus
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import App from './App.vue'

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(ElementPlus, {locale: zhCn, size: 'small', zIndex: 3000 }); //启用element

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

app.config.globalProperties.$axios = axios;
app.mount('#app');