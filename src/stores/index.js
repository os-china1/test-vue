import { defineStore } from 'pinia'// pinia状态管理器

export const myPinia = defineStore('myPinia', {
    state: () => ({
      userInfos:{
        username:'',//todo，pinia到底怎么使用？admin
        password:'',//123456
        roleIds: [],//用户管理密码显示隐藏要用到
        time:'',
      },
      routes: [],//路由菜单条目
      breadcrumbs: [],//map对象，key是路由，value是面包屑条目title数组
      titles:[]//当前要显示的面包屑titiles
    }),
    getters: {},//相当于计算属性

    actions: {//在这里定义方法
      setUserInfos(data){
        this.userInfos = data;
      },
      setRoutes(data){
        this.routes = data;
      },
      setBreadcrumbs(data){
        this.breadcrumbs = data;
      },
      setTitles(data){
        this.titles = data;
      },
    }
})