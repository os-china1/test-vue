import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'


const proxyUrl = 'http://localhost:19000';//代理地址
// https://vitejs.dev/config/

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    host: '0.0.0.0', //本地浏览器地址
    port: 8081, //本地浏览器端口
    fs: {
      strict: false, //支持引用除入口目录的文件
    },
    open: true, //是否自动在浏览器打开
    proxy: {//代理地址配置
      '/authentication': {
        target: proxyUrl,
        ws: true,//如果代理websockets,配置这个参数
        changeOrigin: true,//是否跨域
        //rewrite: (path) => path.replace(/^\/authentication/, ''),
      },
      '/api': {
        target: proxyUrl,
        ws: true,
        changeOrigin: true,
        // rewrite: (path) => path.replace(/^\/api/, ''),
      }
    },
  }
})
